﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Momonga.Async
{
    public class AsyncRequest : CustomYieldInstruction
    {
        System.Action<AsyncRequest> m_OnComplete;

        public override bool keepWaiting { get { return !m_IsCompleted; } }

        bool m_IsCompleted;

        public AsyncRequest OnComplete(System.Action<AsyncRequest> onComplete)
        {
            if (m_IsCompleted)
            {
                onComplete(this);
            }
            else
            {
                m_OnComplete = onComplete;
            }
            return this;
        }

        protected void Complete()
        {
            m_IsCompleted = true;
            if (m_OnComplete != null)
            {
                m_OnComplete(this);
                m_OnComplete = null;
            }
        }
    }

    public abstract class AsyncRequest<T> : AsyncRequest
    {
        public T result { get; protected set; }
    }

    public class AsyncResult : AsyncRequest
    {
        public void InvokeComplete()
        {
            Complete();
        }
    }

    public class AsyncResult<T> : AsyncResult
    {
        public T result { get; set; }

        public AsyncResult<T> OnComplete(System.Action<AsyncResult<T>> onComplete)
        {
            base.OnComplete(request => onComplete(request as AsyncResult<T>));
            return this;
        }
    }

    public class ApiRequest<T> : AsyncRequest<T>
    {
        public string uri { get; private set; }

        public ApiRequest(string uri)
        {
            this.uri = uri;
        }

        public IEnumerator GetAsync()
        {
            using (var www = UnityWebRequest.Get(uri))
            {
                yield return www.SendWebRequest();
                if (www.isNetworkError)
                {
                    Debug.LogError(www.error);
                }
                else
                {
                    result = JsonUtility.FromJson<T>(www.downloadHandler.text);
                }
                Complete();
            }
        }
    }
}
