﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Momonga.Pool
{
    public class Pool<T> : IPool where T : Component, IPoolable
    {
        Queue<T> m_Queue = new Queue<T>();

        System.Func<T> m_Factory;

        public void Init(System.Func<T> factory)
        {
            m_Factory = factory;
        }

        public IEnumerator WarmUp(int count, int sleep = -1)
        {
            for (int i = 0; i < count; i++)
            {
                Create();
                if (i % sleep == sleep - 1)
                {
                    yield return null;
                }
            }
        }

        public T Rent()
        {
            if (m_Queue.Count == 0)
            {
                Create();
            }

            var obj = m_Queue.Dequeue();
            obj.OnRent();
            return obj as T;
        }

        public void Return(IPoolable obj)
        {
            m_Queue.Enqueue(obj as T);
            obj.OnReturn();
        }

        void Create()
        {
            var obj = m_Factory();
            obj.gameObject.AddComponent<PoolContext>().Setup(this);
            Return(obj);
        }
    }
}
