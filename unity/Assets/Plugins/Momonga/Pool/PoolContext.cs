﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Momonga.Pool
{
    public class PoolContext : MonoBehaviour
    {
        IPool m_Pool;

        public void Setup(IPool pool)
        {
            m_Pool = pool;
        }

        public void Return(IPoolable poolable)
        {
            m_Pool.Return(poolable);
        }
    }
}
