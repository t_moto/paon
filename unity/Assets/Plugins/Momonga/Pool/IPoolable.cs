﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Momonga.Pool
{
    public interface IPoolable
    {
        void OnRent();

        void OnReturn();
    }
}
