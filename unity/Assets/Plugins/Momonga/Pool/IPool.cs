﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Momonga.Pool
{
    public interface IPool
    {
        void Return(IPoolable obj);
    }
}
