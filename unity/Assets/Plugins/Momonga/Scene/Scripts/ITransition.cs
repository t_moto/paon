﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Momonga.Scene
{
    public interface ITransition
    {
        Coroutine In();

        Coroutine Out();
    }
}
