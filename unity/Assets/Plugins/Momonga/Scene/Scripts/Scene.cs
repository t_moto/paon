﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Momonga;

namespace Momonga.Scene
{
    using UnityScene = UnityEngine.SceneManagement.Scene;
    using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

    public abstract class Scene : MonoBehaviour
    {
        IEnumerator m_CurrentRoutine;

        protected virtual void Update()
        {
            
        }

        public Coroutine Create()
        {
            m_CurrentRoutine = OnCreate();
            return StartCoroutine(m_CurrentRoutine);
        }

        public Coroutine Enter()
        {
            m_CurrentRoutine = OnEnter();
            return StartCoroutine(m_CurrentRoutine);
        }

        public void Stop()
        {
            StopCoroutine(m_CurrentRoutine);
        }

        public Coroutine Pause()
        {
            m_CurrentRoutine = OnPause();
            return StartCoroutine(m_CurrentRoutine);
        }

        public Coroutine Resume()
        {
            m_CurrentRoutine = OnResume();
            return StartCoroutine(m_CurrentRoutine);
        }

        public Coroutine Exit()
        {
            m_CurrentRoutine = OnExit();
            return StartCoroutine(m_CurrentRoutine);
        }

        public Coroutine Terminate()
        {
            m_CurrentRoutine = OnTerminate();
            return StartCoroutine(m_CurrentRoutine);
        }

        protected virtual IEnumerator OnCreate()
        {
            yield break;
        }

        protected virtual IEnumerator OnEnter()
        {
            yield break;
        }

        protected virtual IEnumerator OnPause()
        {
            yield break;
        }

        protected virtual IEnumerator OnResume()
        {
            yield break;
        }

        protected virtual IEnumerator OnExit()
        {
            yield break;
        }

        protected virtual IEnumerator OnTerminate()
        {
            yield break;
        }

        protected void Push<T>(System.Action<T> initializer = null, ITransition transition = null) where T : Scene
        {
            SceneManager.I.Push<T>(initializer, transition);
        }

        protected void Pop()
        {
            Pop(null);
        }

        protected void Pop(System.Action callback)
        {
            SceneManager.I.Pop(this, callback);
        }

        protected void Change<T>(System.Action<T> initializer = null, ITransition transition = null) where T : Scene
        {
            SceneManager.I.Change<T>(this, initializer, transition);
        }

        protected IEnumerator LoadSceneAsync(string sceneName)
        {
            var ope = UnitySceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            yield return ope;
            UnitySceneManager.SetActiveScene(UnitySceneManager.GetSceneByName(sceneName));
        }

        protected IEnumerator UnloadSceneAsync(string sceneName)
        {
            var ope = UnitySceneManager.UnloadSceneAsync(sceneName);
            yield return ope;
        }
    }
}
