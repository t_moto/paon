﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Momonga.Scene
{
    public class EmptyTransition : ITransition
    {
        public void Init()
        {
        }

        public Coroutine In()
        {
            return null;
        }

        public Coroutine Out()
        {
            return null;
        }
    }
}
