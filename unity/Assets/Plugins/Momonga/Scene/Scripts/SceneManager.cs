﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga;

namespace Momonga.Scene
{
    public class SceneManager : SingletonMonoBehaviour<SceneManager>
    {
        Stack<Scene> m_SceneStack = new Stack<Scene>();

        Queue<IEnumerator> m_RoutineQueue = new Queue<IEnumerator>();

        IEnumerator Start()
        {
            while (true)
            {
                while (m_RoutineQueue.Count > 0)
                {
                    yield return m_RoutineQueue.Dequeue();
                }
                yield return 0;
            }
        }

        Scene CreateScene<T>(System.Action<T> initializer) where T : Scene
        {
            var scene = gameObject.AddComponent<T>();
            initializer.SafeInvoke(scene);
            return scene;
        }

        #region Run

        public void Run<T>(System.Action<T> initializer) where T : Scene
        { 
            m_RoutineQueue.Enqueue(RunRoutine<T>(initializer));
        }

        IEnumerator RunRoutine<T>(System.Action<T> initializer) where T : Scene
        {
            m_SceneStack.Push(CreateScene<T>(initializer));
            yield return m_SceneStack.Peek().Create();
            m_SceneStack.Peek().Enter();
        }

        #endregion

        #region Push

        public void Push<T>(System.Action<T> initializer = null, ITransition transition = null) where T : Scene
        {
            m_RoutineQueue.Enqueue(PushRoutine<T>(initializer, transition ?? new EmptyTransition()));
        }

        IEnumerator PushRoutine<T>(System.Action<T> initializer, ITransition transition) where T : Scene
        {
            yield return transition.In();
            yield return m_SceneStack.Peek().Pause();
            m_SceneStack.Peek().enabled = false;
            m_SceneStack.Push(CreateScene<T>(initializer));
            yield return m_SceneStack.Peek().Create();
            yield return transition.Out();
            m_SceneStack.Peek().Enter();
        }

        #endregion

        #region Pop

        public void Pop(Scene src, System.Action callback = null)
        {
            m_RoutineQueue.Enqueue(PopRoutine(src, callback));
        }

        IEnumerator PopRoutine(Scene src, System.Action callback)
        {
            while (m_SceneStack.Peek() != src)
            {
                yield return PopRoutine();
            }
            yield return PopRoutine();
            callback.SafeInvoke();
        }

        IEnumerator PopRoutine()
        {
            yield return m_SceneStack.Peek().Exit();
            yield return m_SceneStack.Peek().Terminate();
            Destroy(m_SceneStack.Pop());
            m_SceneStack.Peek().enabled = true;
            yield return m_SceneStack.Peek().Resume();
        }

        #endregion

        #region Change

        public void Change<T>(Scene src, System.Action<T> initializer = null, ITransition transition = null) where T : Scene
        { 
            m_RoutineQueue.Enqueue(ChangeRoutine<T>(src, initializer, transition ?? new EmptyTransition()));
        }

        IEnumerator ChangeRoutine<T>(Scene src, System.Action<T> initializer, ITransition transition) where T : Scene
        {
            m_SceneStack.Peek().Stop();
            while (m_SceneStack.Peek() != src)
            {
                yield return PopRoutine();
            }
            yield return m_SceneStack.Peek().Exit();
            yield return transition.In();
            yield return m_SceneStack.Peek().Terminate();
            Destroy(m_SceneStack.Pop());
            m_SceneStack.Push(CreateScene<T>(initializer));
            yield return m_SceneStack.Peek().Create();
            yield return transition.Out();
            m_SceneStack.Peek().Enter();
        }

        #endregion
    }
}
