﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Momonga
{
    public abstract class SingletonMonoBehaviour<T> : MonoBehaviour where T : SingletonMonoBehaviour<T>
    {
        private static T instance;

        public static T I
        {
            get
            {
                if (instance == null)
                {
                    throw new System.Exception(string.Format("[SingletonMonoBehaviour] {0}が存在しません", typeof(T)));
                }
                return instance;
            }
        }

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }

            instance = this as T;
            OnInit();
        }

        private void OnDestroy()
        {
            if (instance == this)
            {
                OnFinal();
                instance = null;
            }
        }

        /// <summary>
        /// 初期化
        /// </summary>
        protected virtual void OnInit()
        {
        }

        /// <summary>
        /// 破棄
        /// </summary>
        protected virtual void OnFinal()
        {
        }

        /// <summary>
        /// 有効か
        /// </summary>
        public static bool IsValid()
        {
            return (instance != null);
        }
    }
}
