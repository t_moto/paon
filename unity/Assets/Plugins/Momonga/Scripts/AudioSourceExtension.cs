﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Momonga
{
    public static class AudioSourceExtension
    {
        public static void CopyTo(this AudioSource src, AudioSource dst)
        {
            dst.clip = src.clip;
            dst.outputAudioMixerGroup = src.outputAudioMixerGroup;
            dst.loop = src.loop;
            dst.volume = src.volume;
            dst.pitch = src.pitch;
        }
    }
}
