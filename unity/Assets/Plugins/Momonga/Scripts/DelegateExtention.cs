﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Momonga
{
    public static class DelegateExtention
    {
        public static void SafeInvoke(this System.Action action)
        {
            if (action != null)
            {
                action.Invoke();
            }
        }

        public static void SafeInvoke<T>(this System.Action<T> action, T obj)
        {
            if (action != null)
            {
                action.Invoke(obj);
            }
        }
    }
}
