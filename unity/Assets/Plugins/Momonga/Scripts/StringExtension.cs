﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Momonga
{
    public static class StringExtension
    {
        public static string ToReverse(this string s)
        {
            return new string(s.Reverse().ToArray());
        }
    }
}
