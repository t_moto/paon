﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Momonga
{
    public static class TransformExtension
    {
        public static void CopyTo(this Transform src, Transform dst)
        {
            dst.position = src.position;
            dst.rotation = src.rotation;
            dst.localScale = src.localScale;
        }
    }
}
