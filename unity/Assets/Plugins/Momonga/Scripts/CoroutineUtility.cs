﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Momonga
{
    public static class CoroutineUtility
    {
        public static IEnumerator Parallel(params Coroutine[] coroutines)
        {
            for (int i = 0, length = coroutines.Length; i < length; i++)
            {
                yield return coroutines[i];
            }
            yield break;
        }
    }
}
