﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga;
using Momonga.Pool;

namespace App
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioObject : MonoBehaviour, IPoolable
    {
        AudioSource m_AudioSource;

        void Awake()
        {
            m_AudioSource = GetComponent<AudioSource>();
        }

        void Update()
        {
            if (Application.isFocused && !m_AudioSource.isPlaying)
            {
                Destroy();
            }
        }

        public void Apply(Transform transform, AudioSource source)
        {
            transform.CopyTo(this.transform);
            source.CopyTo(m_AudioSource);
        }

        public void Play()
        {
            m_AudioSource.Play();
        }

        public void Stop()
        {
            m_AudioSource.Stop();
        }

        #region IPoolable

        public void OnRent()
        {
            gameObject.SetActive(true);
        }

        public void OnReturn()
        {
            gameObject.SetActive(false);
        }

        void Destroy()
        {
            GetComponent<PoolContext>().Return(this);
        }

        #endregion
    }
}
