﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga;
using Momonga.Pool;

namespace App
{
    public class AudioManager : SingletonMonoBehaviour<AudioManager>
    {
        [SerializeField]
        AudioObject m_AudioObjectPrefab;

        [SerializeField]
        AudioSource m_BGMSource;

        [SerializeField]
        AudioClip[] m_BGMClips = new AudioClip[(int)BGMType.Max];

        Pool<AudioObject> m_AudioObjectPool = new Pool<AudioObject>();

        protected override void OnInit()
        {
            m_AudioObjectPool.Init(CreateAudioObject);
        }

        IEnumerator Setup()
        {
            yield return m_AudioObjectPool.WarmUp(10);
        }

        public void PlayBGM(BGMType type)
        {
            var clip = m_BGMClips[(int)type];
            if (clip != m_BGMSource.clip)
            {
                m_BGMSource.clip = clip;
                m_BGMSource.Play();
            }
        }

        public void PlayClipAtPoint(Transform transform, AudioSource src)
        {
            var dst = m_AudioObjectPool.Rent();
            dst.Apply(transform, src);
            dst.Play();
        }

        AudioObject CreateAudioObject()
        {
            return Instantiate(m_AudioObjectPrefab, transform);
        }
    }
}
