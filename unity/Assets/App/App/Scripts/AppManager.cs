﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga;
using App.Game;

namespace App
{
    public class AppManager : SingletonMonoBehaviour<AppManager>
    {
        const string PlayerPrefsKeySkipTitle = "AppManager.skipTitle";

        [SerializeField]
        GameLevelTable m_LevelTable;

        public GameLevelTable levelTable { get { return m_LevelTable; } }

        public event System.Action onReboot;

        public event System.Action onTitle;

        public bool skipTitle
        {
            get
            {
                return PlayerPrefs.HasKey(PlayerPrefsKeySkipTitle);
            }
            set
            {
                if (value)
                {
                    PlayerPrefs.SetInt(PlayerPrefsKeySkipTitle, 1);
                }
                else
                {
                    PlayerPrefs.DeleteKey(PlayerPrefsKeySkipTitle);
                }
            }
        }

        public void Reboot()
        {
            onReboot.SafeInvoke();
        }

        public void Title()
        {
            onTitle.SafeInvoke();
        }
    }
}
