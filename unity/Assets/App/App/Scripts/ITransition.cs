﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
    public interface ITransition
    {
        void Init();

        Coroutine In();

        Coroutine Out();
    }
}
