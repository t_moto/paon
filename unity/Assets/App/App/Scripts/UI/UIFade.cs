﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace App
{
    [RequireComponent(typeof(CanvasGroup))]
    public class UIFade : UITransition
    {
        [Range(0f, 1f)]
        public float from = 0;

        [Range(0f, 1f)]
        public float to = 1f;

        public float duration = .3f;

        CanvasGroup m_CanvasGroup;

        void Awake()
        {
            m_CanvasGroup = GetComponent<CanvasGroup>();
        }

        public override void Init()
        {
            m_CanvasGroup.alpha = from;
            gameObject.SetActive(false);
        }

        protected override IEnumerator OnIn()
        {
            yield return DOTween.To(() => m_CanvasGroup.alpha, x => m_CanvasGroup.alpha = x, to, duration).WaitForCompletion();
        }

        protected override IEnumerator OnOut()
        {
            yield return DOTween.To(() => m_CanvasGroup.alpha, x => m_CanvasGroup.alpha = x, from, duration).WaitForCompletion();
        }
    }
}
