﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace App
{
    public interface IUITransitionEventHandler :  IEventSystemHandler
    {
        void OnBeforeIn();

        void OnAfterIn();

        void OnBeforeOut();

        void OnAfterOut();
    }
}
