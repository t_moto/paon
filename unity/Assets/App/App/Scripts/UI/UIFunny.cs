﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
    public class UIFunny : MonoBehaviour
    {
        void Update()
        {
            var fx = Mathf.Sin(Time.realtimeSinceStartup * Mathf.PI * 2) * .5f + .5f;
            var fy = Mathf.Sin((Time.realtimeSinceStartup + .4f) * Mathf.PI * 2) * .5f + .5f;
            transform.localScale = new Vector3(1f + .04f * fx, 1f + .04f * fy, 1f);
        }
    }
}
