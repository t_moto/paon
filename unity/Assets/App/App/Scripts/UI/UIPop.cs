﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace App
{
    public class UIPop : UITransition
    {
        public Vector3 from = Vector3.zero;

        public Vector3 to = Vector3.one;

        public float duration = .3f;

        public bool stayActive;

        public override void Init()
        {
            transform.localScale = from;
            gameObject.SetActive(stayActive);
        }

        protected override IEnumerator OnIn()
        {
            yield return transform.DOScale(to, duration).SetEase(Ease.OutBack).WaitForCompletion();
        }

        protected override IEnumerator OnOut()
        {
            yield return transform.DOScale(from, duration).SetEase(Ease.InBack).WaitForCompletion();
        }

        protected override void OnAfterOut()
        {
            gameObject.SetActive(stayActive);
        }
    }
}
