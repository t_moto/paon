﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
    [RequireComponent(typeof(CanvasGroup))]
    public class UIPanel : UIObject
    {
        CanvasGroup m_CanvasGroup;

        protected override void Awake()
        {
            m_CanvasGroup = GetComponent<CanvasGroup>();
            m_CanvasGroup.interactable = false;
            base.Awake();
        }            

        public override void OnAfterIn()
        {
            m_CanvasGroup.interactable = true;
        }

        public override void OnBeforeOut()
        {
            m_CanvasGroup.interactable = false;
        }
    }
}
