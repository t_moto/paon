﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace App
{
    [RequireComponent(typeof(AudioSource))]
    public class UITransitionAudio : MonoBehaviour, IUITransitionEventHandler
    {
        [SerializeField]
        AudioClip m_InClip;

        public AudioClip inClip
        {
            get { return m_InClip; }
            set { m_InClip = value; }
        }

        [SerializeField]
        AudioClip m_OutClip;

        public AudioClip outClip
        {
            get { return m_OutClip; }
            set { m_OutClip = value; }
        }

        AudioSource m_AudioSource;

        void Awake()
        {
            m_AudioSource = GetComponent<AudioSource>();
        }

        public void OnBeforeIn()
        {
            if (m_InClip != null)
            {
                m_AudioSource.clip = m_InClip;
                AudioManager.I.PlayClipAtPoint(transform, m_AudioSource);
            }
        }

        public void OnAfterIn()
        {
            
        }

        public void OnBeforeOut()
        {
            if (m_OutClip != null)
            {
                m_AudioSource.clip = m_OutClip;
                AudioManager.I.PlayClipAtPoint(transform, m_AudioSource);
            }
        }

        public void OnAfterOut()
        {
            
        }

        #if UNITY_EDITOR

        void OnValidate()
        {
            GetComponent<AudioSource>().enabled = false;
        }

        #endif
    }
}
