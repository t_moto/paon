﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
    public class UIObject : MonoBehaviour, IUITransitionEventHandler
    {
        UITransition m_Transition;

        protected virtual void Awake()
        {
            m_Transition = GetComponent<UITransition>();
        }

        protected virtual void Start()
        {
            m_Transition.Init();
        }

        public virtual Coroutine In()
        {
            return m_Transition.In();
        }

        public virtual Coroutine Out()
        {
            return m_Transition.Out();
        }

        public virtual void OnBeforeIn()
        {
        }

        public virtual void OnAfterIn()
        {
        }

        public virtual void OnBeforeOut()
        {
        }

        public virtual void OnAfterOut()
        {
        }
    }
}
