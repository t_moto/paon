﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace App
{
    public class UIReaction : MonoBehaviour, IPointerDownHandler
    {
        AudioSource m_AudioSource;

        void Awake()
        {
            m_AudioSource = GetComponent<AudioSource>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Play();
        }

        public void Play()
        {
            transform.DOKill(true);
            transform.DOPunchScale(Vector3.one * .2f, 3f, 3, 1);
            if (m_AudioSource != null)
            {
                m_AudioSource.Play();
            }
        }
    }
}
