﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace App
{
    public abstract class UITransition : MonoBehaviour, ITransition
    {
        public abstract void Init();

        public Coroutine In()
        {
            OnBeforeIn();
            ExecuteEvents.Execute<IUITransitionEventHandler>(gameObject, null, (x, y) => x.OnBeforeIn());
            return StartCoroutine(InRoutine());
        }

        public Coroutine Out()
        {
            OnBeforeOut();
            ExecuteEvents.Execute<IUITransitionEventHandler>(gameObject, null, (x, y) => x.OnBeforeOut());
            return StartCoroutine(OutRoutine());
        }

        IEnumerator InRoutine()
        {
            yield return OnIn();
            OnAfterIn();
            ExecuteEvents.Execute<IUITransitionEventHandler>(gameObject, null, (x, y) => x.OnAfterIn());
        }

        IEnumerator OutRoutine()
        {
            yield return OnOut();
            OnAfterOut();
            ExecuteEvents.Execute<IUITransitionEventHandler>(gameObject, null, (x, y) => x.OnAfterOut());
        }

        protected virtual void OnBeforeIn()
        {
            gameObject.SetActive(true);
        }

        protected virtual IEnumerator OnIn()
        {
            yield break;
        }

        protected virtual void OnAfterIn()
        {
        }

        protected virtual void OnBeforeOut()
        {
        }

        protected virtual IEnumerator OnOut()
        {
            yield break;
        }

        protected virtual void OnAfterOut()
        {
            gameObject.SetActive(false);
        }
    }
}

