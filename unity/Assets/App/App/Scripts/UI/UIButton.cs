﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace App
{
    public class UIButton : MonoBehaviour, IPointerDownHandler
    {
        public void OnPointerDown(PointerEventData eventData)
        {
            transform.DOKill(true);
            transform.DOPunchRotation(Vector3.forward * 20, .5f);
        }
    }
}
