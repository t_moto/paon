﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace App
{
    [RequireComponent(typeof(Graphic))]
    public class UIRainbow : MonoBehaviour
    {
        public float speed;

        [Range(0f, 1f)]
        public float saturation = .5f;

        Graphic m_Graphic;

        void Awake()
        {
            m_Graphic = GetComponent<Graphic>();
        }

        void Update()
        {
            m_Graphic.color = Color.HSVToRGB(Time.time * speed % 1f, saturation, 1f);
        }
    }
}
