﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace App
{
    public class Timer
    {
        readonly Dictionary<float, UnityEvent> m_ListenerMap = new Dictionary<float, UnityEvent>();

        List<float> m_RemoveKeys = new List<float>();

        float m_Timer;

        bool m_Playing;

        public void Play()
        {
            m_Playing = true;
        }

        public void Stop()
        {
            m_ListenerMap.Clear();
            m_Timer = 0f;
            m_Playing = false;
        }

        public void Update()
        {
            if (m_Playing)
            {
                m_Timer += Time.deltaTime;
                TriggerEvent();
            }
        }

        void TriggerEvent()
        {
            m_RemoveKeys.Clear();
            foreach (var kvp in m_ListenerMap)
            {
                if (m_Timer >= kvp.Key)
                {
                    kvp.Value.Invoke();
                    m_RemoveKeys.Add(kvp.Key);
                }
            }
            foreach (var key in m_RemoveKeys)
            {
                m_ListenerMap.Remove(key);
            }
        }

        public void AddListener(float time, UnityAction Listener)
        {
            if (!m_ListenerMap.ContainsKey(time))
            {
                m_ListenerMap.Add(time, new UnityEvent());
            }
            m_ListenerMap[time].AddListener(Listener);
        }
    }
}
