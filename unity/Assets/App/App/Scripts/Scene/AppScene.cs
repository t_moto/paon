﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga.Scene;
using App.Common;
using App.Title;

namespace App
{
    public abstract class AppScene : Scene
    {
        public virtual bool usePauseBlur
        {
            get
            {
                return true;
            }
        }

        protected override IEnumerator OnPause()
        {
            if (usePauseBlur)
            {
                yield return CameraManager.I.Snap();
                UIManager.I.blur.In();
            }
        }

        protected override IEnumerator OnResume()
        {
            if (usePauseBlur)
            {
                yield return CameraManager.I.Snap();
                UIManager.I.blur.Out();
            }
        }
    }
}
