﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using App.Main;

namespace App
{
    public interface IStandAlone
    {
        void Boot(BootScene bootScene);
    }

    public abstract class StandAlone<T> : MonoBehaviour, IStandAlone where T : RootScene
    {
        void Awake()
        {
            var main = SceneManager.GetSceneByName("Main");
            if (!main.isLoaded)
            {
                BootScene.standAlone = this;
                SceneManager.LoadScene("Main");
            }
        }

        public void Boot(BootScene bootScene)
        {
            bootScene.InvokeBoot<T>(Initializer);
        }

        public abstract void Initializer(T scene);
    }
}
