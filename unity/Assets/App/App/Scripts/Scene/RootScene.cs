﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga.Scene;
using App.Common;
using App.Title;

namespace App
{
    public abstract class RootScene : AppScene
    {
        protected override IEnumerator OnCreate()
        {
            AppManager.I.onTitle += OnTitle;
            yield break;
        }

        protected override IEnumerator OnTerminate()
        {
            AppManager.I.onTitle -= OnTitle;
            yield break;
        }

        void OnTitle()
        {
            Change<TitleScene>(null, UICommon.I.loading);
        }
    }
}
