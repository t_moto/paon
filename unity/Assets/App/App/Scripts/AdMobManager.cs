﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using GoogleMobileAds.Api;
using Momonga;
using Momonga.Async;

namespace App
{
    public class AdMobManager : SingletonMonoBehaviour<AdMobManager>
    {
        const string PlayerPrefsKeyLastRewardedAt = "AdMobManager.lastRewardedAt";

        const float RewardTime = 1800;

        BannerView bannerView;

        RewardBasedVideoAd rewardBasedVideo;

        AdRequest request;

        AsyncResult<bool> m_AsyncResult;

        DateTime lastRewardedAt
        {
            get
            {
                if (PlayerPrefs.HasKey(PlayerPrefsKeyLastRewardedAt))
                {
                    return DateTime.FromBinary(Convert.ToInt64(PlayerPrefs.GetString(PlayerPrefsKeyLastRewardedAt)));
                }
                return DateTime.MinValue;
            }
            set
            {
                PlayerPrefs.SetString(PlayerPrefsKeyLastRewardedAt, value.ToBinary().ToString());
            }
        }

        public IEnumerator Setup()
        {
            #if UNITY_ANDROID
            string appId = AppManager.I.levelTable.androidAdmobInfo.appId;
            #elif UNITY_IPHONE
            string appId = AppManager.I.levelTable.iosAdmobInfo.appId;
            #else
            string appId = "unexpected_platform";
            #endif

            #if UNITY_ANDROID
            string adUnitId = AppManager.I.levelTable.androidAdmobInfo.bannerAdUnitId;
            #elif UNITY_IPHONE
            string adUnitId = AppManager.I.levelTable.iosAdmobInfo.bannerAdUnitId;
            #else
            string adUnitId = "unexpected_platform";
            #endif

            MobileAds.Initialize(appId);

            bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
            rewardBasedVideo = RewardBasedVideoAd.Instance;
            rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
            rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
            rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
            rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
            rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
            rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
            rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

            request = new AdRequest.Builder()
                .AddTestDevice(AdRequest.TestDeviceSimulator)
                .AddTestDevice("1b69424daa3bdc5a3a43a51a787a282a")
                .Build();

            yield break;
        }

        protected override void OnFinal()
        {
            if (bannerView != null)
            {
                bannerView.Destroy();
            }

            if (rewardBasedVideo != null)
            {
                rewardBasedVideo.OnAdLoaded -= HandleRewardBasedVideoLoaded;
                rewardBasedVideo.OnAdFailedToLoad -= HandleRewardBasedVideoFailedToLoad;
                rewardBasedVideo.OnAdOpening -= HandleRewardBasedVideoOpened;
                rewardBasedVideo.OnAdStarted -= HandleRewardBasedVideoStarted;
                rewardBasedVideo.OnAdRewarded -= HandleRewardBasedVideoRewarded;
                rewardBasedVideo.OnAdClosed -= HandleRewardBasedVideoClosed;
                rewardBasedVideo.OnAdLeavingApplication -= HandleRewardBasedVideoLeftApplication;
            }
        }

        public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
        {
            rewardBasedVideo.Show();
        }

        public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            m_AsyncResult.result = false;
            m_AsyncResult.InvokeComplete();
        }

        public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
        {
        }

        public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
        {
        }

        public void HandleRewardBasedVideoRewarded(object sender, Reward args)
        {
            Reward();
        }

        public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
        {
            m_AsyncResult.result = true;
            m_AsyncResult.InvokeComplete();
        }

        public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
        {
        }

        public void ShowBanner()
        {
            if (!IsRewarded())
            {
                bannerView.LoadAd(request);
                bannerView.Show();
            }
        }

        public void HideBanner()
        {
            bannerView.Hide();
        }

        public AsyncResult<bool> ShowVideo()
        {
            m_AsyncResult = new AsyncResult<bool>();

            #if UNITY_ANDROID
            string adUnitId = AppManager.I.levelTable.androidAdmobInfo.rewardAdUnitId;
            #elif UNITY_IPHONE
            string adUnitId = AppManager.I.levelTable.iosAdmobInfo.rewardAdUnitId;
            #else
            string adUnitId = "unexpected_platform";
            #endif

            rewardBasedVideo.LoadAd(request, adUnitId);

            return m_AsyncResult;
        }

        void Reward()
        {
            lastRewardedAt = DateTime.Now;
            bannerView.Hide();
        }

        bool IsRewarded()
        {
            var span = DateTime.Now - lastRewardedAt;
            return (span.TotalSeconds <= RewardTime);
        }
    }
}
