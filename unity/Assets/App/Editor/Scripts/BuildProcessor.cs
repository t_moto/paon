﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Build;
using App;

public class BuildProcessor : IPreprocessBuild
{
    public int callbackOrder
    {
        get
        {
            return 0;
        }
    }

    public void OnPreprocessBuild(UnityEditor.BuildTarget target, string path)
    {
        var appManager = AssetDatabase.LoadAssetAtPath<AppManager>("Assets/App/Common/Prefabs/AppManager.prefab");
        PlayerSettings.bundleVersion = appManager.levelTable.version;
        PlayerSettings.productName = appManager.levelTable.productName;
        PlayerSettings.applicationIdentifier = appManager.levelTable.packageName;
        PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, new Texture2D[]{ appManager.levelTable.icon });
    }
}
