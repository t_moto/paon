﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga;

namespace App.Common
{
    public class UIManager : SingletonMonoBehaviour<UIManager>
    {
        [SerializeField]
        Canvas m_DeactiveCanvas;

        [SerializeField]
        Canvas m_ActiveCanvas;

        [SerializeField]
        UIBlur m_Blur;

        public UIBlur blur { get { return m_Blur; } }

        public IEnumerator Setup()
        {
            blur.Setup(CameraManager.I.capture.texture);
            yield break;
        }

        public T Add<T>(T prefab) where T : UIPanel
        {
            var panel = Instantiate(prefab, m_ActiveCanvas.transform);
            return panel as T;
        }

        public void Remove<T>(T instance) where T : UIPanel
        {
            Destroy(instance.gameObject);
        }

        public void Active(UIPanel panel)
        {
            panel.transform.SetParent(m_ActiveCanvas.transform);
        }

        public void Deactive(UIPanel panel)
        {
            panel.transform.SetParent(m_DeactiveCanvas.transform);
        }
    }
}
