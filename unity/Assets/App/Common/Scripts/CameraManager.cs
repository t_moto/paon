﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga;

namespace App.Common
{
    public class CameraManager : SingletonMonoBehaviour<CameraManager>
    {
        [SerializeField]
        Camera m_ActiveCamera;

        [SerializeField]
        Camera m_DeactiveCamera;

        [SerializeField]
        Capture m_Capture;

        public Capture capture { get { return m_Capture; } }

        public IEnumerator Setup()
        {
            yield break;
        }

        public Coroutine Snap()
        {
            return m_Capture.Snap();
        }
    }
}
