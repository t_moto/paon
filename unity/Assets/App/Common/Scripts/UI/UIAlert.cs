﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Momonga;

namespace App.Common
{
    public class UIAlert : UIPanel
    {
        [SerializeField]
        Text m_Title;

        [SerializeField]
        Text m_Message;

        public event System.Action onOk;

        public void Setup(string title, string message)
        {
            m_Title.text = title;
            m_Message.text = message;
        }

        public void OnOk()
        {
            onOk.SafeInvoke();
        }
    }
}
