﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace App.Common
{
    public class UIBlur : UIPanel
    {
        [SerializeField]
        RawImage m_Image;

        int m_Count;

        public void Setup(RenderTexture texture)
        {
            m_Image.texture = texture;
        }

        public override Coroutine In()
        {
            m_Count++;
            if (m_Count == 1)
            {
                return base.In();
            }
            return null;
        }

        public override Coroutine Out()
        {
            m_Count--;
            if (m_Count == 0)
            {
                return base.Out();
            }
            return null;
        }
    }
}
