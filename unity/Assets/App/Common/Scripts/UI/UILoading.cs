﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Momonga.Scene;
using App;

namespace App.Common
{
    public class UILoading : UIPanel, Momonga.Scene.ITransition
    {
        [SerializeField]
        Image m_Logo;

        public IEnumerator Setup(Sprite logo)
        {
            m_Logo.sprite = logo;
            m_Logo.SetNativeSize();
            yield break;
        }
    }
}
