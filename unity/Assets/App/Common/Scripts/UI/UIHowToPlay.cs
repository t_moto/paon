﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace App.Common
{
    public class UIHowToPlay : UIPanel
    {
        public event System.Action onClose;

        [SerializeField]
        Image m_Image;

        public IEnumerator Setup(Sprite howToPlay)
        {
            m_Image.sprite = howToPlay;
            yield break;
        }

        public void OnClose()
        {
            onClose.Invoke();
        }
    }
}
