﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace App.Common
{
    [RequireComponent(typeof(Text))]
    public class UILoad : MonoBehaviour
    {
        [SerializeField]
        float m_Interval = .5f;

        Text m_Text;

        string m_Message;

        void Awake()
        {
            m_Text = GetComponent<Text>();
            m_Message = m_Text.text;
            Hide();
        }

        void Update()
        {
            var count = (int)(Time.realtimeSinceStartup / m_Interval) % 4;
            string[] tmp = new string[count + 1];
            m_Text.text = m_Message + string.Join(".", tmp);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
