﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga;

namespace App.Common
{
    public class UICommon : SingletonMonoBehaviour<UICommon>
    {
        [SerializeField]
        UILoading m_Loading;

        public UILoading loading { get { return m_Loading; } }

        [SerializeField]
        UILoad m_Load;

        public UILoad load { get { return m_Load; } }

        public IEnumerator Setup()
        {
            yield return m_Loading.Setup(AppManager.I.levelTable.logo);
        }
    }
}
