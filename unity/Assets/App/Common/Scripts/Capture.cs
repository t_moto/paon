﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Common
{
    public class Capture : MonoBehaviour
    {
        [SerializeField]
        RenderTexture m_RenderTexture;

        public RenderTexture texture { get { return m_RenderTexture; } }

        [SerializeField]
        Material m_BlurMaterial;

        bool m_SnapRequest;

        void Awake()
        {
            m_RenderTexture = RenderTexture.GetTemporary(Screen.width, Screen.height, 0);
        }

        void OnPreRender()
        {
            if (m_SnapRequest)
            {
                OnCapture();
                m_SnapRequest = false;
            }
        }

        public Coroutine Snap()
        {
            return StartCoroutine(SnapRoutine());
        }

        public IEnumerator SnapRoutine()
        {
            m_SnapRequest = true;
            yield return new WaitWhile(() => m_SnapRequest);
        }

        #region ブラー

        [Range(0, 2)]
        public int downsample = 1;

        public enum BlurType
        {
            StandardGauss = 0,
            SgxGauss = 1,
        }

        [Range(0.0f, 10.0f)]
        public float blurSize = 3.0f;

        [Range(1, 4)]
        public int blurIterations = 2;

        public BlurType blurType = BlurType.StandardGauss;

        public Shader blurShader = null;

        [SerializeField]
        private Material blurMaterial = null;

        void OnCapture()
        {
            Graphics.Blit(null, m_RenderTexture);

            float widthMod = 1.0f / (1.0f * (1 << downsample));

            blurMaterial.SetVector("_Parameter", new Vector4(blurSize * widthMod, -blurSize * widthMod, 0.0f, 0.0f));
            m_RenderTexture.filterMode = FilterMode.Bilinear;

            int rtW = m_RenderTexture.width >> downsample;
            int rtH = m_RenderTexture.height >> downsample;

            // downsample
            RenderTexture rt = RenderTexture.GetTemporary(rtW, rtH, 0, m_RenderTexture.format);

            rt.filterMode = FilterMode.Bilinear;
            Graphics.Blit(m_RenderTexture, rt, blurMaterial, 0);

            var passOffs = blurType == BlurType.StandardGauss ? 0 : 2;

            for (int i = 0; i < blurIterations; i++)
            {
                float iterationOffs = (i * 1.0f);
                blurMaterial.SetVector("_Parameter", new Vector4(blurSize * widthMod + iterationOffs, -blurSize * widthMod - iterationOffs, 0.0f, 0.0f));

                // vertical blur
                RenderTexture rt2 = RenderTexture.GetTemporary(rtW, rtH, 0, m_RenderTexture.format);
                rt2.filterMode = FilterMode.Bilinear;
                Graphics.Blit(rt, rt2, blurMaterial, 1 + passOffs);
                RenderTexture.ReleaseTemporary(rt);
                rt = rt2;

                // horizontal blur
                rt2 = RenderTexture.GetTemporary(rtW, rtH, 0, m_RenderTexture.format);
                rt2.filterMode = FilterMode.Bilinear;
                Graphics.Blit(rt, rt2, blurMaterial, 2 + passOffs);
                RenderTexture.ReleaseTemporary(rt);
                rt = rt2;
            }

            Graphics.Blit(rt, m_RenderTexture);

            RenderTexture.ReleaseTemporary(rt);
        }

        #endregion
    }
}
