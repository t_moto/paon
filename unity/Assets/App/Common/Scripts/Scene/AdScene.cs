﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Common
{
    public class AdScene : AppScene
    {
        protected override IEnumerator OnEnter()
        {
            UICommon.I.load.Show();
            var request = AdMobManager.I.ShowVideo();
            yield return request;
            UICommon.I.load.Hide();
            if (!request.result)
            {
                Push<AlertScene>(scene => scene.Init("", "ロードに失敗しました。\n通信環境をお確かめの上、\n再度お試しください。", Pop));
            }
            else
            {
                Pop();
            }
        }
    }
}
