﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga.Scene;

namespace App.Common
{
    public class AlertScene : PopupScene<UIAlert>
    {
        public string title;

        public string message;

        public System.Action onOk;

        protected override string resourcePath
        {
            get
            {
                return "UI/Alert";
            }
        }

        public void Init(string title, string message, System.Action onOk)
        {
            this.title = title;
            this.message = message;
            this.onOk = onOk;
        }

        protected override IEnumerator OnCreate()
        {
            yield return base.OnCreate();
            m_UIPanel.Setup(title, message);
            m_UIPanel.onOk += OnOk;
        }

        protected override IEnumerator OnTerminate()
        {
            m_UIPanel.onOk -= OnOk;
            yield return base.OnTerminate();
        }

        public void OnOk()
        {
            Pop(onOk);
        }
    }
}
