﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga.Scene;

namespace App.Common
{
    public class ConfirmScene : PopupScene<UIConfirm>
    {
        public string title;

        public string message;

        public System.Action onYes;

        public System.Action onNo;

        public ConfirmScene(string title, string message, System.Action onYes, System.Action onNo)
        {
            this.title = title;
            this.message = message;
            this.onYes = onYes;
            this.onNo = onNo;
        }

        protected override string resourcePath
        {
            get
            {
                return "UI/Confirm";
            }
        }

        protected override IEnumerator OnCreate()
        {
            yield return base.OnCreate();
            m_UIPanel.Setup(title, message);
            m_UIPanel.onYes += OnYes;
            m_UIPanel.onNo += OnNo;
        }

        protected override IEnumerator OnTerminate()
        {
            m_UIPanel.onYes -= OnYes;
            m_UIPanel.onNo -= OnNo;
            yield return base.OnTerminate();
        }

        public void OnYes()
        {
            Pop(onYes);
        }

        public void OnNo()
        {
            Pop(onNo);
        }
    }
}
