﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Common
{
    public class HowToPlayScene : PopupScene<UIHowToPlay>
    {
        protected override string resourcePath
        { 
            get
            { 
                return "UI/HowToPlay";
            }
        }

        protected override IEnumerator OnCreate()
        {
            yield return base.OnCreate();

            yield return m_UIPanel.Setup(AppManager.I.levelTable.howToPlay);
            m_UIPanel.onClose += Pop;
        }

        protected override IEnumerator OnTerminate()
        {
            m_UIPanel.onClose -= Pop;
            yield return base.OnTerminate();
        }
    }
}
