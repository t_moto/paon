﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga.Scene;

namespace App.Common
{
    public abstract class PopupScene<T> : AppScene where T : UIPanel
    {
        protected T m_UIPanel;

        protected abstract string resourcePath { get; }

        protected override IEnumerator OnCreate()
        {
            var req = Resources.LoadAsync<T>(resourcePath);
            yield return req;
            m_UIPanel = UIManager.I.Add(req.asset as T);
        }

        protected override IEnumerator OnEnter()
        {
            yield return m_UIPanel.In();
        }

        protected override IEnumerator OnPause()
        {
            UIManager.I.Deactive(m_UIPanel);
            yield return base.OnPause();
        }

        protected override IEnumerator OnResume()
        {
            UIManager.I.Active(m_UIPanel);
            yield return base.OnResume();
        }

        protected override IEnumerator OnExit()
        {
            yield return m_UIPanel.Out();
        }

        protected override IEnumerator OnTerminate()
        {
            UIManager.I.Remove(m_UIPanel);
            yield break;
        }
    }
}
