﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga.Scene;

namespace App.Main
{
    public class Main : MonoBehaviour
    {
        void Awake()
        {
            gameObject.AddComponent<SceneManager>();
            SceneManager.I.Run<BootScene>(scene => {});
        }
    }
}
