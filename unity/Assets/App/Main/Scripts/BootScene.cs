﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga.Scene;
using App.Common;
using App.Title;
using App.Game;

namespace App.Main
{
    public class BootScene : Scene
    {
        public static RootScene bootScene;

        public static IStandAlone standAlone;

        protected override IEnumerator OnCreate()
        {
            yield return LoadSceneAsync("Common");
            yield return CameraManager.I.Setup();
            yield return AdMobManager.I.Setup();
            yield return UIManager.I.Setup();
            yield return UICommon.I.Setup();

            AppManager.I.onReboot += OnReboot;
        }

        protected override IEnumerator OnEnter()
        {
            if (standAlone != null)
            {
                standAlone.Boot(this);
                standAlone = null;
            } else {
                Boot();
            }
            yield break;
        }

        protected override IEnumerator OnTerminate()
        {
            AppManager.I.onReboot -= OnReboot;
            yield break;
        }

        void Boot()
        {
            if (AppManager.I.skipTitle)
            {
                Push<GameScene>(null, UICommon.I.loading);
            }
            else
            {
                Push<TitleScene>(null, UICommon.I.loading);
            }
        }

        public void InvokeBoot<T>(System.Action<T> initializer) where T : RootScene
        {
            Push<T>(initializer, UICommon.I.loading);
        }

        void OnReboot()
        {
            Change<BootScene>();
        }
    }
}
