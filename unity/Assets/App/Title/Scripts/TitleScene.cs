﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using App.Common;
using App.Game;

namespace App.Title
{
    public class TitleScene : RootScene
    {
        protected override IEnumerator OnCreate()
        {
            yield return base.OnCreate();
            yield return LoadSceneAsync("Title");
            yield return UITitle.I.Setup();
            UITitle.I.onPlay += OnPlay;
            UITitle.I.onHowToPlay += OnHowToPlay;
        }

        protected override IEnumerator OnEnter()
        {
            yield return base.OnEnter();
            AudioManager.I.PlayBGM(BGMType.Title);
            AppManager.I.skipTitle = false;
        }

        protected override IEnumerator OnTerminate()
        {
            UITitle.I.onPlay -= OnPlay;
            UITitle.I.onHowToPlay -= OnHowToPlay;
            yield return UnloadSceneAsync("Title");
            yield return base.OnTerminate();
        }

        void OnPlay()
        {
            Change<GameScene>(null, UICommon.I.loading);
            AppManager.I.skipTitle = true;
        }

        void OnHowToPlay()
        {
            Push<HowToPlayScene>();
        }
    }
}
