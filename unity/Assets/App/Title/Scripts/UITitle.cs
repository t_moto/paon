﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Momonga;
using App.Game;

namespace App.Title
{
    public class UITitle : SingletonMonoBehaviour<UITitle>
    {
        public event System.Action onPlay;

        public event System.Action onHowToPlay;

//        [SerializeField]
        GameLevelTable m_LevelTable = null;

        [SerializeField]
        Text m_Title;

        [SerializeField]
        Image m_Bg;

        [SerializeField]
        RawImage m_Picture;

        public IEnumerator Setup()
        {
            m_LevelTable = AppManager.I.levelTable;
            SetupTitle();
            SetupBg();
            SetupPicture();
            yield break;
        }

        void SetupTitle()
        {
            m_Title.text = m_LevelTable.title;
        }

        void SetupBg()
        {
            m_Bg.color = m_LevelTable.color;
        }

        void SetupPicture()
        {
            var rand = Random.Range(0, m_LevelTable.levels.Count);
            var level = m_LevelTable.levels[rand];
            m_Picture.texture = level.picture;
            m_Picture.GetComponent<AspectRatioFitter>().aspectRatio = (float)level.picture.width / (float)level.picture.height;
        }

        public void OnPlay()
        {
            onPlay.SafeInvoke();
        }

        public void OnHowToPlay()
        {
            onHowToPlay.SafeInvoke();
        }
    }
}
