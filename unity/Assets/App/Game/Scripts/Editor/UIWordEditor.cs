﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace App.Game
{
    [CustomEditor(typeof(UIWord))]
    public class UIWordEditor : Editor
    {
        void OnSceneGUI()
        {
            var t = target as UIWord;
            Handles.color = Color.green;
            Handles.DrawWireDisc(t.transform.position, t.transform.forward, t.radius * t.transform.lossyScale.z);
        }
    }
}
