﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace App.Game
{
    public class UIThemePicture : MonoBehaviour
    {
        [SerializeField]
        RawImage m_Image;

        public void Setup(Texture2D picture)
        {
            m_Image.texture = picture;
            m_Image.GetComponent<AspectRatioFitter>().aspectRatio = (float)picture.width / (float)picture.height;
        }
    }
}
