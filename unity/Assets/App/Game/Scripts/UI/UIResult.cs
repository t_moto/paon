﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace App.Game
{
    public class UIResult : UIPanel
    {
        [SerializeField]
        Text m_Cry;

        [SerializeField]
        GameObject m_Success;

        [SerializeField]
        GameObject m_Failed;

        [SerializeField]
        AudioClip m_SuccessClip;

        [SerializeField]
        AudioClip m_FailedClip;

        public event System.Action onClose;

        UITransitionAudio m_TransitionAudio;

        protected override void Awake()
        {
            m_TransitionAudio = GetComponent<UITransitionAudio>();
            base.Awake();
        }

        public IEnumerator Setup(bool isSuccess, GameLevelModel level)
        {
            m_Cry.gameObject.SetActive(isSuccess);
            m_Success.SetActive(isSuccess);
            m_Failed.SetActive(!isSuccess);
            m_TransitionAudio.inClip = isSuccess ? m_SuccessClip : m_FailedClip;
            m_Cry.text = level.word;
            yield break;
        }

        public override void OnAfterIn()
        {
            if (m_Cry.gameObject.activeSelf)
            {
                AudioManager.I.PlayClipAtPoint(transform, m_Cry.GetComponent<AudioSource>());
            }
        }

        public void OnClose()
        {
            onClose.Invoke();
        }
    }
}
