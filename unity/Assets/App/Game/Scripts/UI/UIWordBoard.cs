﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Game
{
    public class UIWordBoard : UITransition
    {
        public event System.Action<int> onSelectBegin = delegate {};

        public event System.Action<int> onSelectDrag = delegate {};

        public event System.Action onSelectEnd = delegate {};

        GameMap m_Map;

        [SerializeField]
        UIWord m_UIWordPrefab = null;

        UIWord[] m_UIWords;

        bool m_Controllable;

        bool m_IsDragging;

        AudioSource m_AudioSource;

        void Awake()
        {
            m_AudioSource = GetComponent<AudioSource>();
        }

        void Start()
        {
            Init();
        }

        void Update()
        {
            if (m_IsDragging && Input.GetMouseButtonUp(0))
            {
                OnBoardUp();
            }
        }

        public IEnumerator Setup(GameMap map)
        {
            m_Map = map;
            CreateWords();
            yield break;
        }

        void CreateWords()
        {
            m_UIWords = new UIWord[m_Map.length];
            for (int y = 0; y < m_Map.row; y++)
            {
                for (int x = 0; x < m_Map.col; x++)
                {
                    var idx = y * m_Map.col + x;
                    var word = m_Map[y, x];
                    m_UIWords[idx] = CreateWord(idx, word);
                }
            }
        }

        UIWord CreateWord(int idx, string word)
        {
            var ui = Instantiate(m_UIWordPrefab, transform);
            ui.Setup(idx, word);
            ui.onPointerDown += () => OnWordDown(ui);
            ui.onPointerEnter += () => OnWordEnter(ui);
            return ui;
        }

        public void Hint()
        {
            foreach (var idx in m_Map.hints)
            {
                m_UIWords[idx].Hint();
            }
        }

        public UIWord GetWord(int idx)
        {
            return m_UIWords[idx];
        }

        public Coroutine PlayStart()
        {
            return StartCoroutine(PlayStartRoutine());
        }

        IEnumerator PlayStartRoutine()
        {
            foreach (var uiWord in m_UIWords)
            {
                uiWord.In();
                yield return new WaitForSeconds(.05f);
            }
        }

        #region イベント

        void OnWordDown(UIWord word)
        {
            if (m_Controllable)
            {
                onSelectBegin.Invoke(word.index);
                m_IsDragging = true;
                m_AudioSource.Play();
            }
        }

        void OnWordEnter(UIWord word)
        {
            if (m_Controllable && m_IsDragging)
            {
                onSelectDrag.Invoke(word.index);
                m_AudioSource.Play();
            }
        }

        void OnBoardUp()
        {
            if (m_Controllable)
            {
                onSelectEnd.Invoke();
                m_IsDragging = false;
            }
        }

        #endregion

        #region ITransition

        public override void Init()
        {
            gameObject.SetActive(false);
        }

        protected override IEnumerator OnIn()
        {
            foreach (var uiWord in m_UIWords)
            {
                uiWord.In();
                m_AudioSource.Play();
                yield return new WaitForSeconds(.03f);
            }
        }

        protected override void OnAfterIn()
        {
            m_Controllable = true;
        }

        protected override void OnBeforeOut()
        {
            m_Controllable = false;
        }

        protected override IEnumerator OnOut()
        {
            foreach (var uiWord in m_UIWords)
            {
                uiWord.Out();
                m_AudioSource.Play();
                yield return new WaitForSeconds(.01f);
            }
        }

        #endregion
    }
}
