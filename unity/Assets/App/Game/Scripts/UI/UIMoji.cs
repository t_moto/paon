﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace App.Game
{
    [RequireComponent(typeof(Text))]
    public class UIMoji : UITransition
    {
        public Vector3 from = Vector3.zero;

        public Vector3 to = Vector3.one;

        public float duration = .3f;

        public bool isActive { get { return gameObject.activeSelf; } }

        void Start()
        {
            Init();
        }

        public void Setup(string word)
        {
            gameObject.SetActive(!string.IsNullOrEmpty(word));
            GetComponent<Text>().text = word;
        }

        public override void Init()
        {
            transform.localScale = from;
        }

        protected override IEnumerator OnIn()
        {
            yield return transform.DOScale(to, duration).SetEase(Ease.OutBack).WaitForCompletion();
        }

        protected override IEnumerator OnOut()
        {
            yield return transform.DOScale(from, duration).SetEase(Ease.InBack).WaitForCompletion();
        }

        protected override void OnAfterOut()
        {
        }
    }
}
