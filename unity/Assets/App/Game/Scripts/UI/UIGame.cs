﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Momonga;

namespace App.Game
{
    [RequireComponent(typeof(CanvasGroup))]
    public class UIGame : SingletonMonoBehaviour<UIGame>
    {
        public event System.Action onMenu;
        public event System.Action onHowToPlay;
        public event System.Action onReload;
        public event System.Action onRetry;

        [SerializeField]
        Canvas m_Canvas;

        public Canvas canvas { get { return m_Canvas; } }

        [SerializeField]
        UITheme m_Theme;

        public UITheme theme { get { return m_Theme; } }

        [SerializeField]
        UIWordBoard m_WordBoard;

        public UIWordBoard wordBoard { get { return m_WordBoard; } }

        [SerializeField]
        UISelect m_Select;

        public UISelect select { get { return m_Select; } }

        [SerializeField]
        UIObject m_Retry;

        public UIObject retry { get { return m_Retry; } }

        [SerializeField]
        Image m_Bg;

        CanvasGroup m_CanvasGroup;

        public bool controllable {
            get { return m_CanvasGroup.interactable && m_CanvasGroup.blocksRaycasts; }
            set { m_CanvasGroup.interactable = m_CanvasGroup.blocksRaycasts = value; }
        }

        protected override void OnInit()
        {
            m_CanvasGroup = GetComponent<CanvasGroup>();
        }

        public IEnumerator Setup()
        {
            SetupBg();
            yield return m_Theme.Setup(GameManager.I.level, GameManager.I.levelTable.showHint);
            yield return m_WordBoard.Setup(GameManager.I.map);
            yield return m_Select.Setup(GameManager.I.select);
        }

        void SetupBg()
        {
            m_Bg.color = GameManager.I.levelTable.color;
        }

        public void OnMenu()
        {
            onMenu.SafeInvoke();
        }

        public void OnHowToPlay()
        {
            onHowToPlay.SafeInvoke();
        }

        public void OnReload()
        {
            onReload.SafeInvoke();
        }

        public void OnRetry()
        {
            onRetry.SafeInvoke();
        }
    }
}
