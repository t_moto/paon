﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace App.Game
{
    public class UITheme : UIObject
    {
        [SerializeField]
        UIThemePicture m_Picture;

        [SerializeField]
        Vector3 m_ResultPos;

        [SerializeField]
        UIMoji[] m_Mojis;

        [SerializeField]
        Text m_Hint;

        AudioSource m_AudioSource;

        protected override void Awake()
        {
            m_AudioSource = GetComponent<AudioSource>();
            base.Awake();
        }

        public IEnumerator Setup(GameLevelModel level, bool showHint)
        {
            m_Picture.Setup(level.picture);
            SetupMojis(level.word);
            SetupHint(level.word, showHint);
            yield break;
        }

        void SetupMojis(string word)
        {
            for (int i = 0, length = m_Mojis.Length; i < length; i++)
            {
                var w = (i < word.Length) ? word[i].ToString() : string.Empty;
                m_Mojis[i].Setup(w);
            }
        }

        void SetupHint(string word, bool showHint)
        {
            m_Hint.text = word;
            m_Hint.gameObject.SetActive(showHint);
        }

        public Coroutine PlayResult()
        {
            return StartCoroutine(PlayResultRoutine());
        }

        IEnumerator PlayResultRoutine()
        {
            yield return transform.DOLocalMove(m_ResultPos, .5f).SetEase(Ease.OutCubic).WaitForCompletion();

            for (int i = 0, length = m_Mojis.Length; i < length; i++)
            {
                if (m_Mojis[i].isActive)
                {
                    m_Mojis[i].In();
                    m_AudioSource.Play();
                    yield return new WaitForSeconds(.3f);
                }
            }
        }
    }
}
