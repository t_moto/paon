﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace App.Game
{
    public class UISelect : MonoBehaviour
    {
        GameSelect m_Select;

        public Vector2 m_StartPos;

        public Vector2 m_EndPos;

        public IEnumerator Setup(GameSelect select)
        {
            m_Select = select;
            m_Select.selectIndexes.ObserveAdd().Subscribe(x => OnChangeWords()).AddTo(this);
            m_Select.selectIndexes.ObserveRemove().Subscribe(x => OnChangeWords()).AddTo(this);
            m_Select.selectIndexes.ObserveReset().Subscribe(x => OnChangeWords()).AddTo(this);
            Draw();
            yield break;
        }

        void OnChangeWords()
        {
            if (m_Select.selectIndexes.Count > 0)
            {
                var firstUI = UIGame.I.wordBoard.GetWord(m_Select.selectIndexes.First());
                var lastUI = UIGame.I.wordBoard.GetWord(m_Select.selectIndexes.Last());
                m_StartPos = transform.parent.InverseTransformPoint(firstUI.transform.position);
                m_EndPos = transform.parent.InverseTransformPoint(lastUI.transform.position);
            }
            Draw();
        }

        void Draw()
        {
            gameObject.SetActive(m_Select.selectIndexes.Count > 0);

            var head = m_EndPos - m_StartPos;
            transform.localPosition = Vector2.Lerp(m_StartPos, m_EndPos, .5f);
            transform.rotation = Quaternion.FromToRotation(Vector2.right, head);
            (transform as RectTransform).sizeDelta = Vector2.right * head.magnitude;
        }
    }
}
