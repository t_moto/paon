﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using Momonga;

namespace App.Game
{
    public class UIWord : UIObject, IPointerDownHandler, IPointerEnterHandler, IPointerUpHandler, ICanvasRaycastFilter
    {
        [SerializeField]
        Text m_Self;

        [SerializeField]
        float m_Radius = 50f;

        public float radius { get { return m_Radius; } }

        public event System.Action onPointerDown;

        public event System.Action onPointerEnter;

        public event System.Action onPointerUp;

        public int index { get; private set; }

        public void Setup(int index, string word)
        {
            this.index = index;
            m_Self.text = word;
        }

        public void Hint()
        {
            gameObject.AddComponent<UIFunny>();
        }

        public override void OnBeforeOut()
        {
            var funny = gameObject.GetComponent<UIFunny>();
            if (funny != null)
            {
                Destroy(funny);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            onPointerDown.Invoke();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            onPointerEnter.Invoke();
            transform.DOKill(true);
            transform.DOPunchRotation(Vector3.forward * 10, .5f).WaitForCompletion();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            onPointerUp.SafeInvoke();
        }

        public bool IsRaycastLocationValid(Vector2 sp, Camera eventCamera)
        {
            return Vector2.Distance(eventCamera.ScreenToWorldPoint(sp), transform.position) / transform.lossyScale.x < m_Radius;
        }
    }
}
