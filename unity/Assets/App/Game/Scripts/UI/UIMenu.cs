﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga;

namespace App.Game
{
    public class UIMenu : UIPanel
    {
        public event System.Action onShow;

        public event System.Action onTitle;

        public event System.Action onClose;

        public void OnShow()
        {
            onShow.SafeInvoke();
        }

        public void OnTitle()
        {
            onTitle.SafeInvoke();
        }

        public void OnClose()
        {
            onClose.SafeInvoke();
        }
    }
}
