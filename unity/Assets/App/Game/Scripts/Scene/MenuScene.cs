﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga.Scene;
using App.Common;

namespace App.Game
{
    public class MenuScene : PopupScene<UIMenu>
    {
        protected override string resourcePath
        {
            get
            {
                return "UI/Menu";
            }
        }

        protected override IEnumerator OnCreate()
        {
            yield return base.OnCreate();
            m_UIPanel.onShow += OnShow;
            m_UIPanel.onTitle += OnTitle;
            m_UIPanel.onClose += Pop;
        }

        protected override IEnumerator OnTerminate()
        {
            m_UIPanel.onShow -= OnShow;
            m_UIPanel.onTitle -= OnTitle;
            m_UIPanel.onClose -= Pop;
            yield return base.OnTerminate();
        }

        public void OnShow()
        {
            Push<AdScene>();
        }

        public void OnTitle()
        {
            AppManager.I.Title();
        }
    }
}
