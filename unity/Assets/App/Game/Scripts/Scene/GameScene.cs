﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using App.Common;

namespace App.Game
{
    public class GameScene : RootScene
    {
        public override bool usePauseBlur
        {
            get
            {
                return false;
            }
        }

        protected override IEnumerator OnCreate()
        {
            yield return base.OnCreate();

            yield return LoadSceneAsync("Game");
            yield return GameManager.I.Setup();
            yield return UIGame.I.Setup();

            UIGame.I.onMenu += OnMenu;
            UIGame.I.onHowToPlay += OnHowToPlay;
            UIGame.I.onReload += OnReload;
            UIGame.I.onRetry += OnRetry;
        }

        protected override IEnumerator OnEnter()
        {
            yield return base.OnEnter();
            AudioManager.I.PlayBGM(BGMType.Game);
            AdMobManager.I.ShowBanner();
            Push<GameStartScene>();
        }

        protected override IEnumerator OnExit()
        {
            AdMobManager.I.HideBanner();
            yield return base.OnExit();
        }

        protected override IEnumerator OnTerminate()
        {
            UIGame.I.onMenu -= OnMenu;
            UIGame.I.onHowToPlay -= OnHowToPlay;
            UIGame.I.onReload -= OnReload;
            UIGame.I.onRetry -= OnRetry;
            yield return UnloadSceneAsync("Game");

            yield return Resources.UnloadUnusedAssets();
            System.GC.Collect();

            yield return base.OnTerminate();
        }

        void OnMenu()
        {
            Push<MenuScene>();
        }

        void OnHowToPlay()
        {
            Push<HowToPlayScene>();
        }

        void OnReload()
        {
            Change<GameScene>(null, UICommon.I.loading);
        }

        void OnRetry()
        {
            Change<GameScene>(null, UICommon.I.loading);
        }
    }
}
