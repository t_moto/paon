﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga;
using App.Common;

namespace App.Game
{
    public class GameResultScene : PopupScene<UIResult>
    {
        public bool isSuccess;

        public System.Action<bool> onPop;

        protected override string resourcePath
        {
            get
            {
                return "UI/Result";
            }
        }

        protected override IEnumerator OnCreate()
        {
            yield return base.OnCreate();
            yield return m_UIPanel.Setup(isSuccess, GameManager.I.level);
        }

        protected override IEnumerator OnEnter()
        {
            yield return base.OnEnter();
			yield return new WaitForSecondsRealtime(isSuccess ? .3f : .1f);
            Pop(() => onPop.SafeInvoke(isSuccess));
        }
    }
}
