﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Game
{
    public class GamePlayScene : GameChildScene
    {
        protected override IEnumerator OnCreate()
        {
            GameManager.I.onJudge += OnJudge;
            yield break;
        }

        protected override IEnumerator OnEnter()
        {
            GameManager.I.Play();
            yield break;
        }

        protected override IEnumerator OnResume()
        {
            GameManager.I.Clear();
            yield return base.OnResume();
        }

        protected override IEnumerator OnExit()
        {
            GameManager.I.Stop();
            yield break;
        }

        protected override IEnumerator OnTerminate()
        {
            GameManager.I.onJudge -= OnJudge;
            yield break;
        }

        void OnJudge(bool isSuccess)
        {
            Push<GameResultScene>(scene => {
                scene.isSuccess = isSuccess;
                scene.onPop = OnResultPop;
            });
        }

        void OnResultPop(bool isSuccess)
        {
            if (isSuccess)
            {
                Change<GameEndScene>();
            }
        }
    }
}
