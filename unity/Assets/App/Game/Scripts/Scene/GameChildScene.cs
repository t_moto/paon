﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga.Scene;
using App.Common;

namespace App.Game
{
    public class GameChildScene : AppScene
    {
        protected override IEnumerator OnPause()
        {
            yield return base.OnPause();
            Time.timeScale = 0f;
        }

        protected override IEnumerator OnResume()
        {
            Time.timeScale = 1f;
            yield return base.OnResume();
        }
    }
}
