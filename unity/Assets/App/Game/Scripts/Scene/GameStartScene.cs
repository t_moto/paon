﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Game
{
    public class GameStartScene : GameChildScene
    {
        protected override IEnumerator OnEnter()
        {
            yield return UIGame.I.theme.In();
            yield return UIGame.I.wordBoard.In();
            Change<GamePlayScene>();
        }
    }
}
