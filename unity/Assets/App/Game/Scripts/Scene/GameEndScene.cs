﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Game
{
    public class GameEndScene : GameChildScene
    {
        protected override IEnumerator OnEnter()
        {
            yield return UIGame.I.wordBoard.Out();
            yield return UIGame.I.theme.PlayResult();
            yield return new WaitForSeconds(.5f);
            yield return UIGame.I.retry.In();
        }
    }
}
