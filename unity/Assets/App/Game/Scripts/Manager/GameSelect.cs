﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;

namespace App.Game
{
    public class GameSelect
    {
        GameManager m_Owner;

        ReactiveCollection<int> m_SelectIndexes = new ReactiveCollection<int>();

        public ReactiveCollection<int> selectIndexes { get { return m_SelectIndexes; } }

        public int count { get { return m_SelectIndexes.Count; } }

        public GameSelect(GameManager owner)
        {
            m_Owner = owner;
        }

        public void SelectBegin(int idx)
        {
            m_SelectIndexes.Add(idx);
        }

        #region SelectDrag

        public void _SelectDrag(int idx)
        {
            // 同じの選択していたらキャンセル
            if (CheckCancel(idx))
            {
                return;
            }

            // 周りにあるワードか
            if (!CheckAround(idx))
            {
                return;
            }

            // 直線上か
            if (!CheckLinear(idx))
            {
                return;
            }

            m_SelectIndexes.Add(idx);
        }

        public void SelectDrag(int idx)
        {
            List<int> idxs;
            if (!CheckLinear2(idx, out idxs))
            {
                return;
            }

            m_SelectIndexes.Clear();
            foreach (var _idx in idxs)
            {
                m_SelectIndexes.Add(_idx);
            }
        }

        bool CheckCancel(int idx)
        {
            if (m_SelectIndexes.Contains(idx))
            {
                for (int i = m_SelectIndexes.Count - 1; i >= 0; i--)
                {
                    var _idx = m_SelectIndexes[i];
                    if (_idx == idx)
                    {
                        break;
                    }
                    m_SelectIndexes.RemoveAt(i);
                }
                return true;
            }
            return false;
        }

        bool CheckAround(int idx)
        {
            return m_Owner.map.IsAround(idx, m_SelectIndexes.Last());
        }

        bool CheckLinear(int idx)
        {
            if (m_SelectIndexes.Count >= 2)
            {
                var prev = m_SelectIndexes.ElementAt(m_SelectIndexes.Count - 1);
                var prev2 = m_SelectIndexes.ElementAt(m_SelectIndexes.Count - 2);
                var prevDiff = prev - prev2;
                var curDiff = idx - prev;
                // 差分が同じなら直線上なはず
                return (curDiff == prevDiff);
            }
            return true;
        }

        bool CheckLinear2(int idx, out List<int> idxs)
        {
            return m_Owner.map.IsLinear(m_SelectIndexes.First(), idx, out idxs);
        }

        #endregion

        public void SelectEnd()
        {
            m_SelectIndexes.Clear();
        }

        public int[] GetIndexes()
        {
            return m_SelectIndexes.ToArray();
        }
    }
}
