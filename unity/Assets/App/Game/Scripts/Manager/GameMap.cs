﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Momonga;

namespace App.Game
{
    public class GameMap
    {
        public enum Align
        {
            タテ,
            ヨコ,
            ナナメ,
            Max
        }

        string[,] m_Map;

        public int[] hints { get; private set; }

        public int length { get { return m_Map.Length; } }

        public int row { get { return m_Map.GetLength(0); } }

        public int col { get { return m_Map.GetLength(1); } }

        public string this [int y, int x] { get { return m_Map[y, x]; } }

        #region Create

        public void Create(GameLevelModel level, GameLevelModel dummy)
        {
            var map = new string[level.row, level.col];

            var chars = GenerateChars(level.row * level.col, level.word, dummy.word);
            ApplyChars(map, chars);

            var word = LotInvert(level.word);
            var align = LotAlign();
            hints = ApplyWord(map, word, align);

            m_Map = map;
        }

        string[] GenerateChars(int count, string word, string dummy)
        {
            string[] chars = new string[count];
            var words = word + dummy;
            for (int i = 0; i < count; i++)
            {
                chars[i] = words[Random.Range(0, words.Length)].ToString();
            }
            return chars;
        }

        void ApplyChars(string[,] map, string[] chars)
        {
            int row = map.GetLength(0);
            int col = map.GetLength(1);
            for (int y = 0; y < row; y++)
            {
                for (int x = 0; x < col; x++)
                {
                    var idx = y * col + x;
                    map[y, x] = chars[idx];
                    idx++;
                }
            }
        }

        Align LotAlign()
        {
            return (Align)Random.Range(0, (int)Align.Max);
        }

        string LotInvert(string word)
        {
            if (Random.Range(0, 2) == 0)
            {
                return word.ToReverse();
            }
            return word;
        }

        int[] ApplyWord(string[,] map, string word, Align align)
        {
            var result = new int[word.Length];

            int row = map.GetLength(0);
            int col = map.GetLength(1);
            switch (align)
            {
                case Align.タテ:
                    {
                        int x = Random.Range(0, col);
                        int y = Random.Range(0, row - word.Length);
                        for (int i = 0; i < word.Length; i++)
                        {
                            map[y + i, x] = word[i].ToString();
                            var idx = (y + i) * col + x;
                            result[i] = idx;
                        }
                    }
                    break;

                case Align.ヨコ:
                    {
                        int x = Random.Range(0, col - word.Length);
                        int y = Random.Range(0, row);
                        for (int i = 0; i < word.Length; i++)
                        {
                            map[y, x + i] = word[i].ToString();
                            var idx = y * col + (x + i);
                            result[i] = idx;
                        }
                    }
                    break;

                case Align.ナナメ:
                    {
                        int x = Random.Range(0, col - word.Length);
                        int y = Random.Range(0, row - word.Length);
                        for (int i = 0; i < word.Length; i++)
                        {
                            map[y + i, x + i] = word[i].ToString();
                            var idx = (y + i) * col + (x + i);
                            result[i] = idx;
                        }
                    }
                    break;
            }

            return result;
        }

        #endregion

        public bool IsAround(int a, int b)
        {
            var p1 = new Vector2(a % col, a / col);
            var p2 = new Vector2(b % col, b / col);
            var diff = p2 - p1;
            return ((int)Mathf.Abs(diff.x) <= 1 && (int)Mathf.Abs(diff.y) <= 1);
        }

        public bool IsLinear(int a, int b, out List<int> idxs)
        {
            var p1 = new Vector2(a % col, a / col);
            var p2 = new Vector2(b % col, b / col);
            var diff = p2 - p1;

            if ((int)p1.x == (int)p2.x)
            {
                idxs = new List<int>();
                var s = (int)Mathf.Sign(p2.y - p1.y);
                int i = a;
                for (; i != b; i += col * s)
                {
                    idxs.Add(i);
                }
                idxs.Add(i);
                return true;
            }

            if ((int)p1.y == (int)p2.y)
            {
                idxs = new List<int>();
                var s = (int)Mathf.Sign(p2.x - p1.x);
                int i = a;
                for (; i != b; i += 1 * s)
                {
                    idxs.Add(i);
                }
                idxs.Add(i);
                return true;
            }

            if ((int)Mathf.Abs(diff.x) == (int)Mathf.Abs(diff.y))
            {
                idxs = new List<int>();
                var xs = (int)Mathf.Sign(p2.x - p1.x);
                var ys = (int)Mathf.Sign(p2.y - p1.y);
                int i = a;
                for (; i != b; i += 1 * xs + col * ys)
                {
                    idxs.Add(i);
                }
                idxs.Add(i);
                return true;
            }

            idxs = null;
            return false;
        }

        public string GetWord(int[] idxs)
        {
            var word = "";
            for (int i = 0, lenght = idxs.Length; i < lenght; i++)
            {
                var idx = idxs[i];
                var x = idx % col;
                var y = idx / col;
                word += m_Map[y, x];
            }
            return word;
        }
    }
}
