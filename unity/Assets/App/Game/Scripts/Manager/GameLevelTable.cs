﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace App.Game
{
    [CreateAssetMenu()]
    public class GameLevelTable : ScriptableObject
    {
        [System.Serializable]
        public class AdmobInfo
        {
            [Header("AppId")]
            public string appId;

            [Header("バナー広告ID")]
            public string bannerAdUnitId;

            [Header("リワード広告ID")]
            public string rewardAdUnitId;
        }

        [Header("アプリ名")]
        [Multiline]
        public string title = "なぞってアニマル";

        [Header("表示名")]
        public string productName = "なぞアル";

        [Header("パッケージ名")]
        public string packageName = "in.tsurur.paon";

        [Header("バージョン")]
        public string version = "0.0.0";

        [Header("アイコン")]
        public Texture2D icon;

        [Header("ロゴ")]
        public Sprite logo;

        [Header("遊び方")]
        public Sprite howToPlay;

        [Header("背景色")]
        public Color color = Color.white;

        [Header("Android広告情報")]
        public AdmobInfo androidAdmobInfo;

        [Header("iOS広告情報")]
        public AdmobInfo iosAdmobInfo;

        [Header("レベルリスト")]
        public List<GameLevelModel> levels;

        [Header("ヒントを表示する")]
        public bool showHint;

        #if UNITY_EDITOR
        [ContextMenu("3分割")]
        public void Split()
        {
            const int Num = 3;

            var path = AssetDatabase.GetAssetPath(this);
            var fileName = Path.GetFileNameWithoutExtension(path);

            var list = levels.OrderBy(i => System.Guid.NewGuid()).ToList();

            var count = list.Count / Num;
            for (int i = 0; i < Num; i++)
            {
                var no = i + 1;
                var savePath = path.Replace(fileName, fileName + no);
                var table = ScriptableObject.CreateInstance<GameLevelTable>();
                table.levels = list.GetRange(i * count, count);
                AssetDatabase.CreateAsset(table, savePath);
            }

            AssetDatabase.Refresh ();
        }
        #endif
    }
}
