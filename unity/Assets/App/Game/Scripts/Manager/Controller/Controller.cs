﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Game
{
    public abstract class Controller : MonoBehaviour, IController
    {
        protected GameManager m_Owner;

        public void Init(GameManager owner)
        {
            m_Owner = owner;
        }

        public virtual IEnumerator Setup()
        {
            yield break;
        }
    }
}
