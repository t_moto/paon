﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Game
{
    public interface IController
    {
        void Init(GameManager owner);

        IEnumerator Setup();
    }
}
