﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Game
{
    public class UserController : Controller
    {
        public override IEnumerator Setup()
        {
            UIGame.I.wordBoard.onSelectBegin += OnSelectBegin;
            UIGame.I.wordBoard.onSelectDrag += OnSelectDrag;
            UIGame.I.wordBoard.onSelectEnd += OnSelectEnd;
            yield break;
        }

        void OnSelectBegin(int idx)
        {
            m_Owner.SelectBegin(idx);
        }

        void OnSelectDrag(int idx)
        {
            m_Owner.SelectDrag(idx);
        }

        void OnSelectEnd()
        {
            m_Owner.SelectEnd();
        }
    }
}
