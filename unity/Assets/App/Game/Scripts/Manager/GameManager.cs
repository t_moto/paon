﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UniRx;
using Momonga;

namespace App.Game
{
    public class GameManager : SingletonMonoBehaviour<GameManager>
    {
        public event System.Action<bool> onJudge;

        [SerializeField]
        Camera m_Camera;

        new public Camera camera { get { return m_Camera; } }

//        [SerializeField]
        GameLevelTable m_LevelTable;
        public GameLevelTable levelTable { get { return m_LevelTable; } }

        [SerializeField]
        GameLevelModel m_Level;

        public GameLevelModel level { get { return m_Level; } }

        [SerializeField]
        GameMap m_Map;

        public GameMap map { get { return m_Map; } }

        [SerializeField]
        GameSelect m_Select;

        public GameSelect select { get { return m_Select; } }

        Timer m_Timer;

        IController m_Controller;

        protected override void OnInit()
        {
            m_Map = new GameMap();
            m_Select = new GameSelect(this);
            m_Timer = new Timer();
        }

        void Update()
        {
            m_Timer.Update();
        }

        #region Setup

        public IEnumerator Setup()
        {
            m_LevelTable = AppManager.I.levelTable;
            yield return SetupController();
            yield return SetupLevel();
        }

        IEnumerator SetupController()
        {
            // ユーザー操作（チュートリアルなどではAIコントローラにする）
            m_Controller = gameObject.AddComponent<UserController>();
            m_Controller.Init(this);
            yield return m_Controller.Setup();
        }

        IEnumerator SetupLevel()
        {
            m_Level = m_LevelTable.levels[Random.Range(0, m_LevelTable.levels.Count)];

            // 「ー」は分かりづらいので外す
            GameLevelModel dummy = null;
            do
            {
                dummy = m_LevelTable.levels[Random.Range(0, m_LevelTable.levels.Count)];
            } while(dummy == m_Level || dummy.word.IndexOf("ー") != -1);

            m_Map.Create(m_Level, dummy);

            yield break;
        }

        #endregion

        public void Play()
        {
            m_Timer.AddListener(10, Hint);
            m_Timer.Play();
        }

        public void Stop()
        {
            m_Timer.Stop();
        }

        void Hint()
        {
            UIGame.I.wordBoard.Hint();
        }

        public void SelectBegin(int idx)
        {
            m_Select.SelectBegin(idx);
        }

        public void SelectDrag(int idx)
        {
            m_Select.SelectDrag(idx);
        }

        public void SelectEnd()
        {
            if (m_Select.count > 1)
            {
                Judge();
            }
            else
            {
                Clear();
            }
        }

        public void Judge()
        {
            var idxs = m_Select.GetIndexes();
            var word = m_Map.GetWord(idxs);
            var reverse = word.ToReverse();

            bool isSuccess = (word == m_Level.word || reverse == m_Level.word);
            onJudge.SafeInvoke(isSuccess);
        }

        public void Clear()
        {
            m_Select.SelectEnd();
        }
    }
}
