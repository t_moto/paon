﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Game
{
    [System.Serializable]
    public class GameLevelModel
    {
        [Header("ワード")]
        public string word;

        [Header("画像")]
        public Texture2D picture;

        [Header("鳴き声")]
        public string cry;

        [Header("行数")]
        public int row;

        [Header("列数")]
        public int col;
    }
}
